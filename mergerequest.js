'use strict'
const local = require('./local.json')
const consts = require('./consts')
const gitlab = require('./gitlab')
const h = require('./helper')

let logPrefix = null
let gitlabProjectUri = null

module.exports.mergeRequest = (event, context, callback) => {
  logPrefix = null
  gitlabProjectUri = null
  if (!event.headers['X-Gitlab-Token'] || event.headers['X-Gitlab-Token'] !== local.gitlabSecretToken) {
    return h.return200(callback)
  }

  const body = JSON.parse(event.body)
  const mrObject = body.object_attributes
  const branchParts = mrObject.source_branch.split('-')
  if (branchParts[0] !== 'pvc') {
    // automation wont work
    return h.return200(callback)
  }
  const iid = branchParts[1]
  console.log(event.body)

  logPrefix = 'issue iid: ' + iid + ', mr id: ' + mrObject.id + ' --> '
  gitlabProjectUri = consts.GITLAB_API + '/projects/' + mrObject.target_project_id

  if (mrObject.action === 'approve') {
    return h.return200(callback)
  }

  if (mrObject.action === 'merge') {
    console.log(logPrefix + ' the issue is ready for testing')
    return gitlab.addLabelToIssue(consts.FOR_TESTING, mrObject, iid, logPrefix, gitlabProjectUri)
      .then(() => {
        return h.return200(callback)
      })
  }

  decideOnLabels(mrObject, iid, body.changes.title)
    .then(() => {
      if (mrObject.action === 'open') {
        return gitlab.addRelatedIssueToMR(mrObject.description, iid, gitlabProjectUri, mrObject.iid)
      }
      return Promise.resolve()
    })
    .then(() => {
      return h.return200(callback)
    })
    .catch(e => {
      console.log(e)
    })
}

const decideOnLabels = (mrObject, iid, title) => {
  if (mrObject.action === 'open' && !mrObject.work_in_progress) {
    console.log('New merge request. I will change issue labels from ' + consts.IN_PROGRESS + ' to ' + consts.FOR_REVIEW)
    return gitlab.addLabelToIssue(consts.FOR_REVIEW, mrObject, iid, logPrefix, gitlabProjectUri)
  } else if (mrObject.action === 'open' && mrObject.work_in_progress) {
    console.log('New merge request in WIP state. No issue label change')
    return gitlab.addLabelToIssue(consts.IN_PROGRESS, mrObject, iid, logPrefix, gitlabProjectUri)
  } else if (mrObject.action === 'update' && !mrObject.work_in_progress) {
    console.log('Merge request was updated and not WIP anymore. I will change issue labels from ' + consts.IN_PROGRESS + ' to ' + consts.FOR_REVIEW)
    return gitlab.addLabelToIssue(consts.FOR_REVIEW, mrObject, iid, logPrefix, gitlabProjectUri)
  } else if (mrObject.action === 'update' && mrObject.work_in_progress) {
    if (title && (
      (gitlab.isWorkInProgress(title.current) && !gitlab.isWorkInProgress(title.previous)) ||
      (!gitlab.isWorkInProgress(title.current) && gitlab.isWorkInProgress(title.previous))
    )) {
      console.log('Updated merge request to a WIP state. If the issue is in ' + consts.FOR_REVIEW + ', will change to ' + consts.IN_PROGRESS)
      return gitlab.addLabelToIssue(consts.IN_PROGRESS, mrObject, iid, logPrefix, gitlabProjectUri)
    } else {
      console.log('Updated merge request in WIP state. No issue label change')
      return gitlab.addLabelToIssue(consts.IN_PROGRESS, mrObject, iid, logPrefix, gitlabProjectUri)
    }
  } else {
    return Promise.resolve()
  }
}
