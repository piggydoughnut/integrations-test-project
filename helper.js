const return200 = (callback) => {
  const response = {
    statusCode: 200
  }
  callback(null, response)
}

module.exports = {
  return200
}
