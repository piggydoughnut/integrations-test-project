const request = require('request-promise')
const local = require('local.json')
const consts = require('consts')

const authRequest = (uri, body, method) => {
  let options = {
    uri: uri,
    headers: {
      'Private-Token': local.gitlabPrivateTokenAPI
    },
    json: true
  }
  if (method) {
    options.method = method
  }
  if (body) {
    options.body = body
  }
  console.log(options.body)
  return request(options)
}

const closeIssue = (iid, logPrefix, gitlabProjectUri) => {
  console.log(logPrefix + ' closing issue')
  console.log(gitlabProjectUri + '/issues/' + iid + '?state_event=close')
  return authRequest(gitlabProjectUri + '/issues/' + iid + '?state_event=close', null, 'PUT')
}

const isWorkInProgress = (title) => {
  return title.indexOf('WIP') > -1 || title.indexOf('wip') > -1 || title.indexOf('Wip') > -1
}

const getIssue = (uri) => {
  return authRequest(uri)
    .catch(e => console.log(e))
}

const addLabelToIssue = (newLabel, mrObject, iid, logPrefix, gitlabProjectUri) => {
  return getIssue(gitlabProjectUri + '/issues/?iids[]=' + iid)
    .then(issue => {
      if (!issue || !issue[0] || issue.length !== 1) {
        console.log(logPrefix + 'Did not find the issue')
        return Promise.resolve()
      }
      let labels = ''
      if (newLabel) {
        const labelsToRemove = consts.SET_LABELS.filter(label => label === newLabel)
        labels = issue[0].labels.filter(l => labelsToRemove.indexOf(l) > -1)
        labels.push(newLabel)
        if (labels.length > 1) {
          labels = labels.join()
        } else {
          labels = labels[0]
        }
      }
      // https://gitlab.com/api/v4/projects/8040146/issues/1?labels=For review
      return authRequest(gitlabProjectUri + '/issues/' + iid + '?labels=' + labels, null, 'PUT')
    })
    .then(result => {
      return Promise.resolve()
    })
    .catch(e => {
      console.log(e)
    })
}

const addRelatedIssueToMR = (description, iid, gitlabProjectUri, mrid) => {
  description += "\n\n\n Relates #" + iid
  return updateMR(gitlabProjectUri, mrid, {description: description})
}

// PUT /projects/:id/merge_requests/:merge_request_iid
//
const updateMR = (gitlabProjectUri, mrid, body) => {
  return authRequest(gitlabProjectUri + '/merge_requests/' + mrid, body, 'PUT')
}
module.exports = {
  closeIssue,
  authRequest,
  isWorkInProgress,
  addLabelToIssue,
  addRelatedIssueToMR
}
