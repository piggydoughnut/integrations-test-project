const h = require('./helper')
const local = require('./local.json')
const gitlab = require('gitlab')
const consts = require('./consts')

module.exports.issue = (event, context, callback) => {
  if (!event.headers['X-Gitlab-Token'] || event.headers['X-Gitlab-Token'] !== local.gitlabSecretToken) {
    return h.return200(callback)
  }
  const body = JSON.parse(event.body)
  console.log(event.body)
  if (body.labels.length > 0) {
    body.labels.forEach(label => {
      if (label.title === 'Done') {
        const iid = body.object_attributes.iid
        return gitlab.closeIssue(
          iid,
          'issue iid: ' + iid + ' --> ',
          consts.GITLAB_API + '/projects/' + body.object_attributes.project_id
        )
          .then(a => {
            return h.return200(callback)
          })
          .catch(e => console.log(e))
      }
    })
  }
  return h.return200(callback)
}
