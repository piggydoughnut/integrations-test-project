const IN_PROGRESS = 'In progress'
const FOR_REVIEW = 'Code Review'
const FOR_TESTING = 'QA/Testing'
const SET_LABELS = [IN_PROGRESS, FOR_REVIEW, FOR_TESTING]

const GITLAB_API = 'https://gitlab.com/api/v4'

module.exports = {
  IN_PROGRESS,
  FOR_REVIEW,
  FOR_TESTING,
  SET_LABELS,
  GITLAB_API
}
